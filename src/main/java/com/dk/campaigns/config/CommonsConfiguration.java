package com.dk.campaigns.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Caio Andrade (dk)
 */
@Configuration
@ComponentScan(basePackages = {
        "com.dk.campaigns.controller"
})
public class CommonsConfiguration {

}
