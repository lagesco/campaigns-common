package com.dk.campaigns.helper;

import com.dk.campaigns.domain.CampaignResource;

import java.time.LocalDate;

/**
 * Builder para CampaignResource
 *
 * @author Caio Andrade (dk)
 */
public final class CampaignResourceBuilder {
    private String id;
    private String name;
    private String teamId;
    private LocalDate startDate;
    private LocalDate endDate;

    private CampaignResourceBuilder() {
    }

    public static CampaignResourceBuilder create() {
        return new CampaignResourceBuilder();
    }

    public CampaignResourceBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public CampaignResourceBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CampaignResourceBuilder withTeamId(String teamId) {
        this.teamId = teamId;
        return this;
    }

    public CampaignResourceBuilder withStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public CampaignResourceBuilder withEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public CampaignResource build() {
        CampaignResource campaign = new CampaignResource();
        campaign.setId(id);
        campaign.setName(name);
        campaign.setTeamId(teamId);
        campaign.setStartDate(startDate);
        campaign.setEndDate(endDate);
        return campaign;
    }
}
