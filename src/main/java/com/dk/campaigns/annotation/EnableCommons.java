package com.dk.campaigns.annotation;

import com.dk.campaigns.config.CommonsConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Caio Andrade (dk)
 */
@Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@Target(value = { java.lang.annotation.ElementType.TYPE })
@Documented
@Import({CommonsConfiguration.class})
public @interface EnableCommons {
}
