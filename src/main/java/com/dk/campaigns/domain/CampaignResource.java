package com.dk.campaigns.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Entidade representa os dados da Campanha (Campaign).
 *
 * @author Caio Andrade (dk)
 */
@ApiModel(value = "Campanha")
public class CampaignResource implements Serializable {
    @ApiModelProperty(value = "Id", notes = "Id da Campanha", readOnly = true)
    private String id;

    @ApiModelProperty(value = "Nome", notes = "Nome da Campanha", required = true)
    @Size(min=5, max=100, message="Nome tem capacidade de 5 a 100 caracteres.")
    @NotNull(message="Nome da campanha é obrigatória.")
    private String name;

    @Size(min=5, max=100, message="Id do time do coração tem capacidade de 5 a 100 caracteres.")
    @ApiModelProperty(value = "Id time coração", notes = "Id do time do coração", required = true)
    @NotNull(message="Id time do coração é obrigatório.")
    private String teamId;

    @ApiModelProperty(value = "Inicio da vigência", required = true)
    @NotNull(message="O inicio da vigência é obrigatório.")
    private LocalDate startDate;

    @ApiModelProperty(value = "Fim da vigência", required = true)
    @NotNull(message="O fim da vigência é obrigatório.")
    private LocalDate endDate;

    public CampaignResource() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CampaignResource)) return false;
        CampaignResource that = (CampaignResource) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(teamId, that.teamId) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, teamId, startDate, endDate);
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "name='" + name + '\'' +
                ", teamId='" + teamId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
