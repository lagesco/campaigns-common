package com.dk.campaigns.controller.error;


import com.dk.campaigns.domain.ErrorMessage;
import com.dk.campaigns.exception.EntityNotFoundException;
import com.dk.campaigns.exception.EntityRegisteredException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Controle de erros.
 *
 * @author Caio Andrade (dk)
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    public RestResponseEntityExceptionHandler() {
        super();
    }

    // 400

    @ExceptionHandler({ DataIntegrityViolationException.class })
    public ResponseEntity<Object> handleBadRequest(final DataIntegrityViolationException ex, final WebRequest request) {
        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.BAD_REQUEST.toString());
        msg.setMessage(ex.getMessage());

        return handleExceptionInternal(ex, msg, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.BAD_REQUEST.toString());
        msg.setMessage(ex.getMessage());

        // ex.getCause() instanceof JsonMappingException, JsonParseException // for additional information later on
        return handleExceptionInternal(ex, msg, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        //Locale locale = LocaleContextHolder.getLocale();
        String code = ex.getBindingResult().getFieldError().getDefaultMessage();

        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.BAD_REQUEST.toString());
        msg.setMessage(ex.getBindingResult().getFieldError().getDefaultMessage());

        return handleExceptionInternal(ex, msg, headers, HttpStatus.BAD_REQUEST, request);
    }

    // 404

    @ExceptionHandler(value = { EntityNotFoundException.class })
    protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.NOT_FOUND.toString());
        msg.setMessage(ex.getMessage());

        return handleExceptionInternal(ex, msg, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    // 409

    @ExceptionHandler({ InvalidDataAccessApiUsageException.class, DataAccessException.class, EntityRegisteredException.class})
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.CONFLICT.toString());
        msg.setMessage(ex.getMessage());

        return handleExceptionInternal(ex, msg, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // 412

    // 500
    @ExceptionHandler({ NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class })
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        ErrorMessage msg = new ErrorMessage();
        msg.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        msg.setMessage(ex.getMessage());

        return handleExceptionInternal(ex, ex, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
